FROM openjdk:11

ADD ./build/libs/*.jar /app.jar
ENTRYPOINT exec java $JAVA_OPTS -jar app.jar
