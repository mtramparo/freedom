package magalu.freedom.wishlist.domain.wishlist;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface WishlistRepository extends MongoRepository<Wishlist, UUID> {

    @Query(value="{ customerCode: ?0, products: { $elemMatch: { productCode: ?1 } } }", exists = true)
    boolean existsByCustomerCodeAndProductCode(UUID customerCode, UUID productCode);
}
