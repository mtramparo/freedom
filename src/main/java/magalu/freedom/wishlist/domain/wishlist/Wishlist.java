package magalu.freedom.wishlist.domain.wishlist;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.domain.product.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Document(collection = "wishlist")
public class Wishlist {

    @Id
    UUID customerCode;

    Set<Product> products;

    public Set<Product> addProduct(@NonNull final Product product) {
        if (products == null) {
            products = new HashSet<>();
        }
        products.add(product);

        return products;
    }

    public Wishlist removeProduct(@NonNull final UUID productCode) {
        Optional.ofNullable(this.products)
                .orElse(Collections.emptySet())
                .removeIf(product -> Objects.equals(productCode,
                        product.getProductCode()));
        return this;
    }


}
