package magalu.freedom.wishlist.infrastructure;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Builder
@ToString
@AllArgsConstructor(access = PRIVATE)
@FieldDefaults(makeFinal = true, level = PRIVATE)
class CustomErrorResponse implements Serializable {

    private static long serialVersionUID = 7077147922060207675L;

    String message;

    @Singular Map<String, String> errors;
}
