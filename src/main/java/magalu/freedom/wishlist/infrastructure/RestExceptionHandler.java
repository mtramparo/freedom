package magalu.freedom.wishlist.infrastructure;

import magalu.freedom.wishlist.application.wishlist.ProductAmountExceededException;
import magalu.freedom.wishlist.application.wishlist.WishlistNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Classe que configura o tratamento de exceções lançadas durante o processamento de uma requisição
 * HTTP.
 */
@ControllerAdvice
class RestExceptionHandler extends ResponseEntityExceptionHandler {

    static final String FIELD_VALIDATION_FAIL_MSG = "Errors occurred during field validation.";

    static final String GENERIC_ERROR_MSG = "An error occurred while processing the request";

    /**
     * Trata exceções lançadas por erros de Beans Validations
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {
        final BindingResult result = ex.getBindingResult();
        return ResponseEntity.status(status).body(processValidationErrors(result));
    }

    /**
     * Trata exceções lançadas por erros de validação em resources.
     */
    @ExceptionHandler({WishlistNotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(final RuntimeException ex) {
        final var response =
                CustomErrorResponse.builder()
                        .message(GENERIC_ERROR_MSG)
                        .error("detail", ex.getMessage())
                        .build();
        return ResponseEntity.status(NOT_FOUND).body(response);
    }

    /**
     * Trata exceções lançadas por erros de validação em resources.
     */
    @ExceptionHandler({ProductAmountExceededException.class})
    protected ResponseEntity<Object> handleProductAmountExceededException(final RuntimeException ex) {
        final var response =
                CustomErrorResponse.builder()
                        .message(GENERIC_ERROR_MSG)
                        .error("detail", ex.getMessage())
                        .build();
        return ResponseEntity.status(BAD_REQUEST).body(response);
    }

    private static CustomErrorResponse processValidationErrors(final BindingResult result) {
        final var errorResponseBuilder = CustomErrorResponse.builder();
        errorResponseBuilder.message(FIELD_VALIDATION_FAIL_MSG);

        result.getAllErrors()
                .forEach(
                        e -> {
                            final String field;
                            if (e instanceof FieldError) {
                                field = ((FieldError) e).getField();

                            } else {
                                field = e.getObjectName();
                            }
                            errorResponseBuilder.error(field, e.getDefaultMessage());
                        });

        return errorResponseBuilder.build();
    }
}
