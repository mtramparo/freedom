package magalu.freedom.wishlist.intefarces;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductData;
import magalu.freedom.wishlist.application.wishlist.WishlistData;
import magalu.freedom.wishlist.application.wishlist.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/freedom/wishlist/{customerCode}", produces = MediaType.APPLICATION_JSON_VALUE)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor(onConstructor_ = @Autowired)
public class WishlistResource {

    WishlistService wishlistService;

    @GetMapping
    public ResponseEntity<WishlistData> findWishlist(@PathVariable("customerCode") UUID customerCode) {
        return ResponseEntity.ok(wishlistService.findCustomerWishlist(customerCode));
    }

    @GetMapping("/products/{productCode}")
    public ResponseEntity<Boolean> findProductInWishlist(@PathVariable("customerCode") UUID customerCode,
                                                             @PathVariable("productCode") UUID productCode) {
        return ResponseEntity.ok(wishlistService.findProductInWishlist(customerCode, productCode));
    }

    @PostMapping
    public ResponseEntity<WishlistData> insertWishlistProduct(@PathVariable("customerCode") UUID customerCode,
                                                @RequestBody ProductData productData) {
        return ResponseEntity.ok(wishlistService.addProductInWishlist(customerCode, productData));
    }

    @DeleteMapping("/products/{productCode}")
    public ResponseEntity<WishlistData> removeProductInWishlist(@PathVariable("customerCode") UUID customerCode,
                                                  @PathVariable("productCode") UUID productCode) {
        wishlistService.removeProductInWishlist(customerCode, productCode);
        return ResponseEntity.ok().build();
    }


}
