package magalu.freedom.wishlist.application.wishlist;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductData;

import java.util.Set;
import java.util.UUID;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WishlistData {
    UUID customerCode;
    Set<ProductData> products;
}
