package magalu.freedom.wishlist.application.wishlist;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductAssembler;
import magalu.freedom.wishlist.application.product.ProductData;
import magalu.freedom.wishlist.domain.wishlist.Wishlist;
import magalu.freedom.wishlist.domain.wishlist.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor(onConstructor_ = @Autowired, access = AccessLevel.PUBLIC)
public class WishlistService {

    private static final int MAX_PRODUCTS = 20;

    WishlistRepository repository;

    WishlistDataAssembler assembler;

    ProductAssembler productAssembler;

    public WishlistData findCustomerWishlist(@NonNull final UUID customerCode) {
        final var wishlist = repository.findById(customerCode);

        return wishlist
                .map(assembler::assemble)
                .orElseThrow(() -> new WishlistNotFoundException(customerCode));
    }

    public boolean findProductInWishlist(@NonNull final UUID customerCode, @NonNull final UUID productCode) {
        return repository.existsByCustomerCodeAndProductCode(customerCode, productCode);
    }

    public void removeProductInWishlist(@NonNull final UUID customerCode, @NonNull final UUID productCode) {
        repository.findById(customerCode)
                .map(wishlist -> wishlist.removeProduct(productCode))
                .map(repository::save)
                .orElseThrow(() -> new WishlistNotFoundException(customerCode));
    }


    public WishlistData addProductInWishlist(@NonNull final UUID customerCode, @NonNull final ProductData productData) {
        final var optionalWishlist = repository.findById(customerCode);

        Wishlist wishlist;

        if (optionalWishlist.isPresent()) {
            wishlist = optionalWishlist.get();
            final var productSet = wishlist.getProducts();
            if (productSet.size() > MAX_PRODUCTS) {
                throw new ProductAmountExceededException(customerCode);
            }
        } else {
            wishlist = Wishlist.builder().customerCode(customerCode).build();
        }
        wishlist.addProduct(productAssembler.assemble(productData));

        return assembler.assemble(repository.save(wishlist));
    }


}
