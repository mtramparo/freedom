package magalu.freedom.wishlist.application.wishlist;

import java.util.UUID;

public class ProductAmountExceededException extends RuntimeException {

    private static final String DEFAULT_ERROR_MESSAGE = "Product amount exceeded to customerCode=%s.";

    public ProductAmountExceededException(final UUID customerCode) {
        super(String.format(DEFAULT_ERROR_MESSAGE, customerCode.toString()));
    }
}
