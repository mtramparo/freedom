package magalu.freedom.wishlist.application.wishlist;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductDataAssembler;
import magalu.freedom.wishlist.domain.wishlist.Wishlist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor(onConstructor_ = @Autowired)
public class WishlistDataAssembler {

    ProductDataAssembler productDataAssembler;

    public WishlistData assemble(@NonNull final Wishlist wishlist){
        return WishlistData
                .builder()
                .customerCode(wishlist.getCustomerCode())
                .products(productDataAssembler.assembleProductDataSet(wishlist.getProducts()))
                .build();
    }
}
