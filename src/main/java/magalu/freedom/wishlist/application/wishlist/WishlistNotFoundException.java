package magalu.freedom.wishlist.application.wishlist;

import java.util.UUID;

public class WishlistNotFoundException extends RuntimeException{
    private static final String DEFAULT_ERROR_MESSAGE = "Wishlist of customer %s not found or not exists.";

    public WishlistNotFoundException(final UUID customerCode) {
        super(String.format(DEFAULT_ERROR_MESSAGE, customerCode.toString()));
    }
}
