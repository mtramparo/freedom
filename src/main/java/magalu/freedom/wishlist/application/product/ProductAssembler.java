package magalu.freedom.wishlist.application.product;

import lombok.NonNull;
import magalu.freedom.wishlist.domain.product.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductAssembler {

    public Product assemble(@NonNull final ProductData productData){
        return Product.builder()
                .productCode(productData.getProductId())
                .name(productData.getName())
                .price(productData.getPrice())
                .build();
    }
}
