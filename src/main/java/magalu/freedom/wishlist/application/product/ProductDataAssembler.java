package magalu.freedom.wishlist.application.product;

import lombok.NonNull;
import magalu.freedom.wishlist.domain.product.Product;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ProductDataAssembler {

    private ProductData assembleProduct(@NonNull final Product product) {
        return ProductData.builder()
                .productId(product.getProductCode())
                .name(product.getName())
                .price(product.getPrice())
                .build();
    }

    public Set<ProductData> assembleProductDataSet(@NonNull final Set<Product> products) {
        return products.stream()
                .map(this::assembleProduct)
                .collect(Collectors.toSet());

    }
}
