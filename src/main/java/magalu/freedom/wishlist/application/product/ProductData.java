package magalu.freedom.wishlist.application.product;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.UUID;


@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductData {
    @NotNull
    UUID productId;
    @NotBlank
    String name;
    @NotNull
    @Positive
    BigDecimal price;
}
