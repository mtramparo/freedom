package magalu.freedom.wishlist.application.product;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.domain.product.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class ProductDataAssemblerTest {

    private static final UUID PRODUCT1_CODE = UUID.randomUUID();
    private static final String PRODUCT1_NAME = "PRODUCT1_NAME";
    private static final BigDecimal PRODUCT1_PRICE = BigDecimal.valueOf(10);

    private static final UUID PRODUCT2_CODE = UUID.randomUUID();
    private static final String PRODUCT2_NAME = "PRODUCT2_NAME";
    private static final BigDecimal PRODUCT2_PRICE = BigDecimal.valueOf(20);

    private static final UUID PRODUCT3_CODE = UUID.randomUUID();
    private static final String PRODUCT3_NAME = "PRODUCT3_NAME";
    private static final BigDecimal PRODUCT3_PRICE = BigDecimal.valueOf(30);

    @InjectMocks
    ProductDataAssembler productDataAssembler;

    @Test
    void assembleProductDataSet() {
        final var productSet = buildProductSet();
        final var productDataSet = productDataAssembler.assembleProductDataSet(productSet);

        assertEquals(productSet.size(), productDataSet.size());

        final var firstProductData = productDataSet.stream()
                .filter(productData -> productData.getProductId().equals(PRODUCT1_CODE)).findFirst().get();

        assertEquals(PRODUCT1_CODE, firstProductData.getProductId());
        assertEquals(PRODUCT1_NAME, firstProductData.getName());
        assertEquals(PRODUCT1_PRICE, firstProductData.getPrice());

        final var secondProductData = productDataSet.stream()
                .filter(productData -> productData.getProductId().equals(PRODUCT2_CODE)).findFirst().get();

        assertEquals(PRODUCT2_CODE, secondProductData.getProductId());
        assertEquals(PRODUCT2_NAME, secondProductData.getName());
        assertEquals(PRODUCT2_PRICE, secondProductData.getPrice());

        final var thirdProductData = productDataSet.stream()
                .filter(productData -> productData.getProductId().equals(PRODUCT3_CODE)).findFirst().get();

        assertEquals(PRODUCT3_CODE, thirdProductData.getProductId());
        assertEquals(PRODUCT3_NAME, thirdProductData.getName());
        assertEquals(PRODUCT3_PRICE, thirdProductData.getPrice());
    }

    private Set<Product> buildProductSet() {
        final var productSet = new HashSet<Product>();
        productSet.add(Product.builder()
                .productCode(PRODUCT1_CODE)
                .name(PRODUCT1_NAME)
                .price(PRODUCT1_PRICE)
                .build());

        productSet.add(Product.builder()
                .productCode(PRODUCT2_CODE)
                .name(PRODUCT2_NAME)
                .price(PRODUCT2_PRICE)
                .build());

        productSet.add(Product.builder()
                .productCode(PRODUCT3_CODE)
                .name(PRODUCT3_NAME)
                .price(PRODUCT3_PRICE)
                .build());

        return productSet;
    }
}