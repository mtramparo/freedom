package magalu.freedom.wishlist.application.wishlist;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductAssembler;
import magalu.freedom.wishlist.application.product.ProductData;
import magalu.freedom.wishlist.domain.product.Product;
import magalu.freedom.wishlist.domain.wishlist.Wishlist;
import magalu.freedom.wishlist.domain.wishlist.WishlistRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class WishlistServiceTest {
    private static final Integer EXCEEDED_MAX_PRODUCT_SIZE = 21;
    @InjectMocks
    WishlistService service;

    @Mock
    WishlistRepository repository;

    @Mock
    WishlistDataAssembler assembler;

    @Spy
    ProductAssembler productAssembler;

    @Mock
    WishlistData wishlistData;

    @Mock
    Wishlist wishlist;

    @Mock
    ProductData productData;

    @Mock
    Set<Product> productSet;

    @Test
    void findCustomerWishlist_wishlistExists_shouldExecuteSuccessfully() {
        UUID customerCode = UUID.randomUUID();
        when(repository.findById(customerCode)).thenReturn(Optional.of(wishlist));
        when(assembler.assemble(wishlist)).thenReturn(wishlistData);

        service.findCustomerWishlist(customerCode);
    }

    @Test
    void findCustomerWishlist_wishlistNotExists_shouldThrowWishlistNotFoundException() {
        UUID customerCode = UUID.randomUUID();
        when(repository.findById(customerCode)).thenReturn(Optional.empty());

        Assertions.assertThrows(WishlistNotFoundException.class, () -> service.findCustomerWishlist(customerCode));
    }


    @Test
    void findProductInWishlist_ProductExistsInWishlist_ShouldReturnTrue() {
        UUID customerCode = UUID.randomUUID();
        UUID productCode = UUID.randomUUID();
        when(repository.existsByCustomerCodeAndProductCode(customerCode, productCode)).thenReturn(Boolean.TRUE);

        Assertions.assertTrue(service.findProductInWishlist(customerCode, productCode));
    }

    @Test
    void findProductInWishlist_ProductNotExistsInWishlist_ShouldReturnFalse() {
        UUID customerCode = UUID.randomUUID();
        UUID productCode = UUID.randomUUID();
        when(repository.existsByCustomerCodeAndProductCode(customerCode, productCode)).thenReturn(Boolean.FALSE);

        assertFalse(service.findProductInWishlist(customerCode, productCode));
    }

    @Test
    void removeProductInWishlist_WishlistExists_ShouldExecuteSuccessfully() {
        UUID customerCode = UUID.randomUUID();
        UUID productCode = UUID.randomUUID();

        when(wishlist.removeProduct(productCode)).thenReturn(wishlist);
        when(repository.findById(customerCode)).thenReturn(Optional.of(wishlist));
        when(repository.save(wishlist)).thenReturn(wishlist);

        service.removeProductInWishlist(customerCode, productCode);

        verify(repository).save(wishlist);
    }

    @Test
    void addProductInWishlist_WishlistExists_ShouldExecuteSuccessfully() {
        UUID customerCode = UUID.randomUUID();

        when(wishlist.getProducts()).thenReturn(productSet);
        when(repository.findById(customerCode)).thenReturn(Optional.of(wishlist));

        service.addProductInWishlist(customerCode, productData);

        verify(productAssembler).assemble(productData);
        verify(repository).save(wishlist);
    }

    @Test
    void addProductInWishlist_WishlistNotExists_ShouldCreateNewOneAndExecuteSuccessfully() {
        UUID customerCode = UUID.randomUUID();

        when(repository.findById(customerCode)).thenReturn(Optional.empty());

        service.addProductInWishlist(customerCode, productData);

        verify(productAssembler).assemble(productData);
        verify(repository).save(any(Wishlist.class));
    }

    @Test
    void addProductInWishlist_WishlistExistsAndProductSizeExceeded_ShouldThrowProductAmountExceededException() {
        UUID customerCode = UUID.randomUUID();

        when(productSet.size()).thenReturn(EXCEEDED_MAX_PRODUCT_SIZE);
        when(wishlist.getProducts()).thenReturn(productSet);
        when(repository.findById(customerCode)).thenReturn(Optional.of(wishlist));

        Assertions.assertThrows(ProductAmountExceededException.class,
                () -> service.addProductInWishlist(customerCode, productData));

    }
}