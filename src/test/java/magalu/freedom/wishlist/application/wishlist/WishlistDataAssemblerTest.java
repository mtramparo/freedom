package magalu.freedom.wishlist.application.wishlist;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import magalu.freedom.wishlist.application.product.ProductDataAssembler;
import magalu.freedom.wishlist.domain.product.Product;
import magalu.freedom.wishlist.domain.wishlist.Wishlist;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class WishlistDataAssemblerTest {

    @InjectMocks
    WishlistDataAssembler assembler;

    @Mock
    ProductDataAssembler productDataAssembler;

    @Mock
    Wishlist wishlist;

    @Mock
    Set<Product> productSet;

    @Test
    void assemble_passingNonNullObject_ShouldExecuteSuccessfully() {
        UUID customerCode = UUID.randomUUID();

        when(wishlist.getCustomerCode()).thenReturn(customerCode);
        when(wishlist.getProducts()).thenReturn(productSet);
        WishlistData wishlistData = assembler.assemble(wishlist);

        verify(productDataAssembler).assembleProductDataSet(productSet);

        assertEquals(customerCode, wishlistData.getCustomerCode());
    }


}